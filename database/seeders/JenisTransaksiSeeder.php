<?php

namespace Database\Seeders;

use App\Models\JenisTransaksi;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class JenisTransaksiSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        JenisTransaksi::create([
            "transaksi" => 'Tarik Tunai',
        ]);
        JenisTransaksi::create([
            "transaksi" => 'Cek Saldo',
        ]);
        JenisTransaksi::create([
            "transaksi" => 'Pembayaran Listrik / Pulsa	',
        ]);
        JenisTransaksi::create([
            "transaksi" => 'Transfer',
        ]);
    }
}

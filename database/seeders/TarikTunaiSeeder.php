<?php

namespace Database\Seeders;

use App\Models\TarikTunai;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class TarikTunaiSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        TarikTunai::create([
            "user_id" => 1,
            "nominal" => 100000,
        ]);
        TarikTunai::create([
            "user_id" => 2,
            "nominal" => 200000,
        ]);
        TarikTunai::create([
            "user_id" => 3,
            "nominal" => 500000,
        ]);
    }
}

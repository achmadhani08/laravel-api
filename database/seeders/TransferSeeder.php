<?php

namespace Database\Seeders;

use App\Models\Transfer;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class TransferSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Transfer::create([
            "user_id" => 1,
            "rekening_tujuan" => 23456,
            "nominal" => 250000,
        ]);
        Transfer::create([
            "user_id" => 2,
            "rekening_tujuan" => 34567,
            "nominal" => 150000,
        ]);
        Transfer::create([
            "user_id" => 3,
            "rekening_tujuan" => 12345,
            "nominal" => 300000,
        ]);
    }
}

<?php

namespace Database\Seeders;

use App\Models\ListrikPulsa;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class ListrikPulsaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ListrikPulsa::create([
            "user_id" => 1,
            "nomor_pelanggan" => 67890,
            "nominal" => 50000,
        ]);
        ListrikPulsa::create([
            "user_id" => 2,
            "nomor_pelanggan" => 78901,
            "nominal" => 20000,
        ]);
        ListrikPulsa::create([
            "user_id" => 3,
            "nomor_pelanggan" => 89012,
            "nominal" => 100000,
        ]);
    }
}

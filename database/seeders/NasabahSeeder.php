<?php

namespace Database\Seeders;

use App\Models\Nasabah;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class NasabahSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Nasabah::create([
            "username" => "dhani08",
            "password" => Hash::make("password"),
            "nomor_rekening" => 12345,
            "nomor_pelanggan" => 67890,
            "saldo" => 2000000,
        ]);
        Nasabah::create([
            "username" => "user2",
            "password" => Hash::make("password"),
            "nomor_rekening" => 23456,
            "nomor_pelanggan" => 78901,
            "saldo" => 1000000,
        ]);
        Nasabah::create([
            "username" => "user3",
            "password" => Hash::make("password"),
            "nomor_rekening" => 34567,
            "nomor_pelanggan" => 89012,
            "saldo" => 3000000,
        ]);
    }
}

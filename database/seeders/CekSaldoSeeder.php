<?php

namespace Database\Seeders;

use App\Models\CekSaldo;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class CekSaldoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        CekSaldo::create([
            "user_id" => 1,
            "saldo" => 2000000,
        ]);
        CekSaldo::create([
            "user_id" => 2,
            "saldo" => 1000000,
        ]);
        CekSaldo::create([
            "user_id" => 3,
            "saldo" => 3000000,
        ]);
    }
}

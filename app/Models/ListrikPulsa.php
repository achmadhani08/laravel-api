<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ListrikPulsa extends Model
{
    use HasFactory;

    public function nasabah()
    {
        return $this->belongsTo(Nasabah::class);
    }
}

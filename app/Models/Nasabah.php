<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Nasabah extends Model
{
    use HasFactory;

    public function tarik_tunais()
    {
        return $this->hasMany(TarikTunai::class);
    }

    public function cek_saldos()
    {
        return $this->hasMany(CekSaldo::class);
    }

    public function listrik_pulsas()
    {
        return $this->hasMany(ListrikPulsa::class);
    }

    public function transfers()
    {
        return $this->hasMany(Transfer::class);
    }
}

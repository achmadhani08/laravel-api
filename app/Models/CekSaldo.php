<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CekSaldo extends Model
{
    use HasFactory;

    public function nasabah()
    {
        return $this->belongsTo(Nasabah::class);
    }
}
